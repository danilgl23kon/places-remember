const markers = [];
function removeAllMarkers() {
  markers.forEach((marker) => {
    marker.remove();
  });

  // Очистка массива маркеров
  markers.length = 0;
}

const checkbox = document.getElementById('defaultCheck1');
const input = document.getElementById('exampleFormControlInput1');
let current_name_mark = ""
checkbox.addEventListener('change', function() {
  input.disabled = !input.disabled;
});


mapboxgl.accessToken = 'pk.eyJ1IjoiZGlpZHVrMjI4IiwiYSI6ImNsaGJleGE0dDBxemMzamx0azZnYnRwbXkifQ.mmMyDvHJMaOKsfwoxYSiFQ';
    const map = new mapboxgl.Map({
        container: 'map', // container ID
        style: 'mapbox://styles/mapbox/streets-v12', // style URL
        center: [-74.5, 40], // starting position [lng, lat]
        zoom: 9, // starting zoom
    });


const formAddRemember = document.getElementById('add-remember-form');

formAddRemember.addEventListener('submit', async (event) => {
  event.preventDefault();
  const formData = new FormData(formAddRemember);

  // Получение CSRF-токена
  const csrfToken = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  let place = document.querySelector('[placeholder="Красноярск"]').value;
  if (input.disabled) {
    place = current_name_mark
  }
  
  const description = document.querySelector('[id="exampleFormControlTextarea1"]').value;
  try {
    const response = await fetch(window.location.href.toString(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrfToken,
      },
      body: JSON.stringify({"name": place, "description": description}),
    });
    const data = await response.text();
    const jsonData = JSON.parse(data);
    console.log(jsonData);
    window.location.href = '../../accounts/profile/';

  } catch (err) {
    console.error(`Error: ${err}`);
  }
});


const form = document.getElementById('find-place-form');

form.addEventListener('submit', async (event) => {
  event.preventDefault();
  const formData = new FormData(form);

  // Получение CSRF-токена
  const csrfToken = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const place = document.querySelector('[class="form-control"]').value;
  console.log(place);
  try {
    const response = await fetch(window.location.href.toString(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrfToken,
      },
      body: JSON.stringify({"place": place}),
    });
    removeAllMarkers();
    const data = await response.text();
    const jsonData = JSON.parse(data);
    console.log(jsonData);
    const marker = new mapboxgl.Marker()
    .setLngLat([jsonData["long"], jsonData["lat"]])
    .addTo(map);
    map.flyTo({
      center: [jsonData["long"], jsonData["lat"]],
      zoom: 9,
      essential: true,
    });
    current_name_mark = place.toString()
    markers.push(marker);

  } catch (err) {
    console.error(`Error: ${err}`);
  }
});
