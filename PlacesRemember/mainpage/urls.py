from allauth.account.views import LogoutView
from django.urls import path, include


from mainpage.views import (
    ShowMainPageView,
    AddRememberView,
    ProfileView,
    DeleteRememberView
)


URL_PATH_ADD_IMPRESS = "accounts/add-impress/"
URL_PATH_PROFILE = 'accounts/profile/'
URL_PATH_DELETE_REMEMBER = 'accounts/profile/delete-remember/'


urlpatterns = [
    path('', ShowMainPageView.as_view(), name='show_main_page'),
    path(URL_PATH_ADD_IMPRESS, AddRememberView.as_view(), name='add_remember'),
    path(URL_PATH_PROFILE, ProfileView.as_view(), name='profile'),
    path(URL_PATH_DELETE_REMEMBER, DeleteRememberView.as_view(), name='delete_remember'),
    path('accounts/', include('allauth.urls')),
    path('logout', LogoutView.as_view()),
]