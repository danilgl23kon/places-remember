from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View
import geocoder
import json


from mainpage.models import Address


MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoiZGlpZHVrMjI4IiwiYSI6ImNsaGJleGE0dDBxemMzamx0azZnYnRwbXkifQ.mmMyDvHJMaOKsfwoxYSiFQ'
PATH_MAIN_PAGE = "mainpage/index.html"
PATH_PROFILE = "profile/profile.html"
PATH_ADD_REMEMBER = "add_remember/add_impression.html"


class ShowMainPageView(View):
    @staticmethod
    def get(request, *args, **kwargs):
        if request.user.is_authenticated:
            return ProfileView.as_view()(request, *args, **kwargs)
        return render(request, PATH_MAIN_PAGE)


class ProfileView(View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get(request):
        if not request.user.is_authenticated:
            return ShowMainPageView(View).get(request)
        posts = Address.objects.filter(user=request.user)
        return render(request, PATH_PROFILE, {"posts": posts})


class AddRememberView(View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AddRememberView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_geo(request):
        js = json.loads(request.body)
        if "place" not in js:
            return JsonResponse({"lat": 33.12, "long": 23.13})
        g = geocoder.mapbox(str(js["place"]), key=MAPBOX_ACCESS_TOKEN)
        if g.current_result is None:
            return JsonResponse({"lat": 33.12, "long": 23.13})
        return JsonResponse({"lat": g.latlng[0], "long": g.latlng[1]})

    @staticmethod
    def save_address(request):
        user = request.user
        js = json.loads(request.body)
        address = js['name']
        description = js['description']

        new_address = Address.objects.create(
            user=user,
            address=address,
            description=description
        )
        new_address.save()
        return JsonResponse({"result": True})

    @staticmethod
    def get(request):
        return render(request, PATH_ADD_REMEMBER)

    def post(self, request):
        js = json.loads(request.body)
        if "place" in js:
            return self.get_geo(request)
        if "name" in js and "description" in js:
            return self.save_address(request)


class DeleteRememberView(View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteRememberView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def post(request):
        js = json.loads(request.body)

        current_user = request.user
        try:
            address = Address.objects.get(id=int(js["data"]), user=current_user)
            address.delete()
            return JsonResponse({'status': 'success', 'message': 'Address deleted.'})
        except Address.DoesNotExist:
            return JsonResponse({'status': 'error', 'message': 'Address not found.'})