from django.db import models
from django.contrib.auth.models import User


class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    address = models.TextField(max_length=50, blank=True, null=False)
    description = models.TextField(max_length=200, blank=True, null=True)

