from django.test import TestCase, RequestFactory, Client
from django.contrib.auth.models import User, AnonymousUser
import json


from mainpage.models import Address
from mainpage.views import (
    ProfileView,
    PATH_PROFILE,
    PATH_MAIN_PAGE,
)
from mainpage.urls import URL_PATH_PROFILE, URL_PATH_ADD_IMPRESS, URL_PATH_DELETE_REMEMBER


RESPONSE_OK = 200
REDiRECT_OK = 302


class TestViews(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.user = User.objects.create_user(
            username='testuser', email='test@example.com', password='testpassword'
        )
        self.address = Address.objects.create(
            user=self.user,
            address="123 Test Street",
            description="Test description"
        )

    def test_show_main_page_authenticated(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get('/')
        self.assertEqual(response.status_code, RESPONSE_OK)
        self.assertTemplateUsed(response, PATH_PROFILE)

    def test_show_main_page_not_authenticated(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, RESPONSE_OK)
        self.assertTemplateUsed(response, PATH_MAIN_PAGE)

    def test_profile_authenticated(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(f'/{URL_PATH_PROFILE}')
        self.assertEqual(response.status_code, RESPONSE_OK)
        self.assertTemplateUsed(response, PATH_PROFILE)

    def test_profile_not_authenticated(self):
        request = self.factory.get(f'/{URL_PATH_PROFILE}')
        request.user = AnonymousUser()
        response = ProfileView.as_view()(request, request.user)
        self.assertEqual(response.status_code, REDiRECT_OK)

    def test_add_remember(self):
        self.client.login(username='testuser', password='testpassword')
        data = {
            "name": "New Address",
            "description": "New Description"
        }
        response = self.client.post(f'/{URL_PATH_ADD_IMPRESS}', json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, RESPONSE_OK)
        self.assertTrue(Address.objects.filter(address="New Address").exists())

    def test_delete_remember(self):
        self.client.login(username='testuser', password='testpassword')
        data = {
            "data": self.address.id
        }
        response = self.client.post(f'/{URL_PATH_DELETE_REMEMBER}', json.dumps(data),
                                    content_type='application/json')
        self.assertEqual(response.status_code, RESPONSE_OK)
        self.assertFalse(Address.objects.filter(id=self.address.id).exists())